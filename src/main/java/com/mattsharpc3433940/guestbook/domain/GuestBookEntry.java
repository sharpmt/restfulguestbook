package com.mattsharpc3433940.guestbook.domain;


import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table (name = "entries")
public class GuestBookEntry {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column (name = "entry_id")
    public Integer id;

    @Size(min=3, max=20)
    public String user;

    @NotBlank
    @Size(min=3, max=144)
    public String comment;

    public String submitted;

    public GuestBookEntry() {
    }

    public GuestBookEntry (Integer id, String user, String comment, String submitted) {
        this.id = id;
        this.user = user;
        this.comment = comment;
        this.submitted = submitted;

    }

    public Integer getId () {
        return id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getSubmitted (){
        return submitted;
    }

    public void setSubmitted (String submitted){
        this.submitted = dateFormat.format(new Date());

    }

    public String getUser () {
        return user;
    }

    public void setUser (String user) {
        this.user = user;
    }

    public String getComment () {
        return comment;
    }

    public void setComment (String comment) {
        this.comment = comment;
    }

    @Override
    public String toString () {
        return "GuestBookEntry{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
